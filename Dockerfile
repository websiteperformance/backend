﻿FROM harbor.gretzki.ddns.net/ms-cache/dotnet/aspnet:8.0
RUN adduser --system runner
WORKDIR /app
COPY build_output/ ./
EXPOSE 5777
ENV ASPNETCORE_URLS=http://+:5777
USER runner
ENTRYPOINT ["dotnet", "build/Backend.dll"]
