﻿using Backend.Services;

namespace BackendTest;

public class WhenAllBackPressureTests
{
    [Test]
    public async Task WhenAllBackPressure_AllTasksSucceed_ReturnsResults()
    {
        // Arrange
        var tasks = Enumerable.Range(0, 10)
            .Select(Task.FromResult);
        int parallelism = 5;

        // Act
        var results = await tasks.WhenAllBackPressure(parallelism);


        // Assert
        await Assert.That(results.Length()).IsEqualTo(10);
    }

    [Test]
    public async Task WhenAllBackPressure_Foo()
    {
        var tasks = await Enumerable
            .Range(0, 100)
            .Select(Adder)
            .WhenAllBackPressure(10);

        await Assert.That(tasks.First()).IsEqualTo(1);
        await Assert.That(tasks.Last()).IsEqualTo(100);
    }

    private static readonly Func<int, Task<int>> Adder = cnt => Task.Run(() => cnt + 1);

    [Test]
    public async Task WhenAllBackPressure_OneTaskFails_ThrowsAggregateException()
    {
        // Arrange
        var tasks = new List<Task<int>>
        {
            Task.FromResult(1),
            Task.FromResult(2),
            Task.FromException<int>(new InvalidOperationException())
        };
        int parallelism = 2;

        // Act & Assert
        await Assert.ThrowsAsync<AggregateException>(() => tasks.WhenAllBackPressure(parallelism));
    }

    [Test]
    public async Task WhenAllBackPressure_RespectsMaxDegreeOfParallelism()
    {
        // Arrange
        int maxParallelism = 3;
        int currentParallelism = 0;
        int peakParallelism = 0;
        var tasks = Enumerable.Range(0, 100)
            .Select(async i =>
            {
                await Task.Delay(10);
                Interlocked.Increment(ref currentParallelism);
                peakParallelism = Math.Max(peakParallelism, currentParallelism);
                Interlocked.Decrement(ref currentParallelism);
                return i;
            });

        // Act
        var countable = await tasks.WhenAllBackPressure(maxParallelism);

        await Assert.That(countable.Count()).IsEqualTo(100);
        // Assert
        await Assert.That(peakParallelism).IsLessThanOrEqualTo(maxParallelism);
    }

    [Test]
    public async Task WhenAllAsync_ReturnsCorrectResults_And_RespectsParallelism()
    {
        // Arrange
        var source = Enumerable.Range(1, 500);
        Func<int, Task<int>> func = async item =>
        {
            await Task.Delay(10); // Simuliert eine asynchrone Operation
            return item * 2; // Eine Beispiel-Operation
        };

        int maxParallelism = 10;

        // Act
        var results = await source.WhenAllFuncAsync(func, maxParallelism);

        // Assert
        await Assert.That(results.Count()).IsEqualTo(500);
        
        foreach (var result in results)
        {
            await Assert.That(result % 2).IsEqualTo(0);
        }

        await Assert.That(ValidateMaxParallelism(source, maxParallelism)).IsTrue();
    }

    // Diese Methode dient zur Überprüfung der Maximalanzahl gleichzeitiger Ausführungen
    private bool ValidateMaxParallelism(IEnumerable<int> source, int maxParallelism)
    {
        int currentParallelism = 0;
        int peakParallelism = 0;
        var tasks = source.Select(async _ =>
        {
            await Task.Delay(10);
            Interlocked.Increment(ref currentParallelism);
            peakParallelism = Math.Max(peakParallelism, currentParallelism);
            Interlocked.Decrement(ref currentParallelism);
        });

        Task.WhenAll(tasks).Wait();
        return peakParallelism <= maxParallelism;
    }
}