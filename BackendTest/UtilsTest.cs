using Backend.Services;
   
namespace BackendTest;

public class UtilsTest
{
    [Test]
    public async Task IsNullOrEmpty_InputIsNullOrEmptyString_ReturnsTrue()
    {
        string input = "";
        var result = input.IsNullOrEmpty2();
        await Assert.That(result).IsTrue();
    }
    [Test]
    public async Task IsNullOrEmpty_InputIsNullOrEmptyString_ReturnsTrue2()
    {
        string? input = null;
        var result = input.IsNullOrEmpty2();
        await Assert.That(result).IsTrue();
    }

    [Test]
    [MatrixDataSource]
    public async Task IsNullOrEmpty_InputIsNonEmptyString_ReturnsFalse([Matrix("     ", "Hello World")] string input)
    {
        
        var result = input.IsNullOrEmpty2();

        await Assert.That(result).IsFalse();
    }
}