namespace Backend.Services;

public interface IConfigurationHandler
{
    T GetValue<T>(string key);
}