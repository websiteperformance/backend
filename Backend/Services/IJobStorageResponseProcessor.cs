using Backend.Services.Inbound.JobStorageResponse;

namespace Backend.Services;

public interface IJobStorageResponseProcessor
{
    Task ProcessAsync(IAsyncEnumerable<PayloadResponseDto> pAsyncEnumerable);
}