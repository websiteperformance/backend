using System.Text.Json.Serialization;

namespace Backend.Services.Outbound.RemoveFromStorage;

public class RemoveFromStorageProducerResponse
{
    [JsonInclude, JsonPropertyName("isError")]
    public bool IsError;

    [JsonInclude, JsonPropertyName("errorMessage")]
    public string? ErrorMessage;
    
    [JsonInclude, JsonPropertyName("id")] public Guid Id;
}
