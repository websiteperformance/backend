using Backend.Services.Outbound.JobRequest;
using Silverback.Messaging.Configuration;

namespace Backend.Services.Outbound.RemoveFromStorage;

public class RemoveFromStorageEndpointConfigurator(ILogger<RemoveFromStorageEndpointConfigurator> logger,
        IConfigurationHandler configurationHandler)
    : IEndpointsConfigurator
{
    private readonly ILogger<RemoveFromStorageEndpointConfigurator> _logger = logger;

    public void Configure(IEndpointsConfigurationBuilder builder)
    {
        builder
            .AddKafkaEndpoints(endpoints => endpoints
                .Configure(config =>
                {
                    config.BootstrapServers =
                        configurationHandler.GetValue<string>("KAFKA_BOOTSTRAP_SERVER");
                })
                .AddOutbound<PayloadDto>(endpoint =>
                {
                    endpoint
                        .ProduceTo(configurationHandler.GetValue<string>("KAFKA_REMOVESTORAGE_TOPICNAME"))
                        .SerializeAsJson(ser => ser.UseFixedType<PayloadDto>());
                }));
    }
}