using Backend.Services.Outbound.JobRequest;

namespace Backend.Services.Outbound.RemoveFromStorage;

public interface IRemoveFromStorageProducer
{
    Task<RemoveFromStorageProducerResponse> ProduceJobAsync(PayloadDto removeFromStorageDto);
}