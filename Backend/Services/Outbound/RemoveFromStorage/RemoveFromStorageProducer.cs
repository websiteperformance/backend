using Backend.Services.Outbound.JobRequest;
using Silverback.Messaging.Publishing;

namespace Backend.Services.Outbound.RemoveFromStorage;

public class RemoveFromStorageProducer(ILogger<RemoveFromStorageProducer> logger, IPublisher publisher)
    : IRemoveFromStorageProducer
{
    public async Task<RemoveFromStorageProducerResponse> ProduceJobAsync(PayloadDto removeFromStorageDto)
    {
        logger.LogInformation("put a new message to remove some storage data in a topic");
        logger.LogInformation("try to remove id {Id} for type {Type}", removeFromStorageDto.Id,
            removeFromStorageDto.PayloadTypeEnum);
        try
        {
            await publisher.PublishAsync<PayloadDto>(removeFromStorageDto);
            return new RemoveFromStorageProducerResponse
                {IsError = false, ErrorMessage = null, Id = removeFromStorageDto.Id};
        }
        catch (Exception e)
        {
            logger.LogError(e, "an error occured");
            return await Task.FromResult(new RemoveFromStorageProducerResponse
                {IsError = true, ErrorMessage = e.Message, Id = removeFromStorageDto.Id});
        }
    }
}