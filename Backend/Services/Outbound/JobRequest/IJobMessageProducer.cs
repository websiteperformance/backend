namespace Backend.Services.Outbound.JobRequest;

public interface IJobMessageProducer
{
    Task<JobMessageProducerResponse> ProduceJobAsync(JobMessage jobMessage);
}