using Silverback.Messaging.Publishing;

namespace Backend.Services.Outbound.JobRequest;

public class JobMessageProducer(ILogger<JobMessageProducer> logger, IPublisher publisher) : IJobMessageProducer
{
    public async Task<JobMessageProducerResponse> ProduceJobAsync(JobMessage jobMessage)
    {
        logger.LogInformation("put a new job to appropriate topic");
        try
        {
            await publisher.PublishAsync<JobMessage>(jobMessage);
            return new JobMessageProducerResponse {IsError = false, Id = jobMessage.Id, ErrorMessage = null};
        }
        catch (Exception exception)
        {
            logger.LogError(exception, "an error occured");
            return await Task.FromResult(new JobMessageProducerResponse
                {IsError = true, ErrorMessage = exception.Message, Id = jobMessage.Id});
        }
    }
}