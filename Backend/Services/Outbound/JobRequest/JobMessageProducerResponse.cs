using System.Text.Json.Serialization;

namespace Backend.Services.Outbound.JobRequest;

public class JobMessageProducerResponse
{
    [JsonInclude, JsonPropertyName("isError")]
    public bool IsError;

    [JsonInclude, JsonPropertyName("errorMessage")]
    public string? ErrorMessage;

    [JsonInclude, JsonPropertyName("id")] public Guid Id;
};