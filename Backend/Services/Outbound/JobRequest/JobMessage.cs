using System.Text.Json.Serialization;
using Backend.Objects;
using Silverback.Messaging.Messages;

namespace Backend.Services.Outbound.JobRequest;

public class JobMessage
{
    [JsonInclude, JsonPropertyName("id")] public Guid Id;
    [JsonInclude, JsonPropertyName("url")] public string? Url;

    [JsonInclude, JsonPropertyName("userAgent")]
    public string? UserAgent;

    [JsonInclude, JsonPropertyName("width")]
    public int Width;

    [JsonInclude, JsonPropertyName("height")]
    public int Height;

    [JsonInclude, JsonPropertyName("location"), Header("x-check-location")]
    public int Location { get; set; }

    [JsonInclude, JsonPropertyName("withTimings")]
    public bool WithTimings { get; set; }

    [JsonInclude, JsonPropertyName("withPerformanceEntries")]
    public bool WithPerformanceEntries { get; set; }

    [JsonInclude, JsonPropertyName("withScreenshot")]
    public bool WithScreenshot { get; set; }

    [JsonInclude, JsonPropertyName("withTracing")]
    public bool WithTracing { get; set; }
    
    [JsonInclude, JsonPropertyName("withNetworks")]
    public bool WithNetworks { get; set; }


    public static implicit operator JobMessage(WebsiteCheckDocument document) => new()
    {
        Height = document.Height,
        UserAgent = document.UserAgent,
        Width = document.Width,
        Id = document.SiteCheckId,
        Location = (int) document.Location,
        Url = document.Url,
        WithScreenshot = document.WithScreenshot,
        WithTracing = document.WithTracing,
        WithPerformanceEntries = document.WithPerformanceEntries,
        WithTimings = document.WithTimings,
        WithNetworks = document.WithNetworks
    };
}