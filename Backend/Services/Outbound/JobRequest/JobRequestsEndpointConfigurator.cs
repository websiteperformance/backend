using Silverback.Messaging.Configuration;

namespace Backend.Services.Outbound.JobRequest;

public class JobRequestsEndpointConfigurator(ILogger<JobRequestsEndpointConfigurator> logger,
        IConfigurationHandler configurationHandler)
    : IEndpointsConfigurator
{
    private readonly ILogger<JobRequestsEndpointConfigurator> _logger = logger;

    public void Configure(IEndpointsConfigurationBuilder builder)
    {
        builder
            .AddKafkaEndpoints(endpoints => endpoints
                .Configure(config =>
                {
                    config.BootstrapServers =
                        configurationHandler.GetValue<string>("KAFKA_BOOTSTRAP_SERVER");
                })
                .AddOutbound<JobMessage>(endpoint =>
                {
                    endpoint
                        .ProduceTo(configurationHandler.GetValue<string>("KAFKA_JOBREQUESTS_TOPICNAME"))
                        .SerializeAsJson(ser => ser.UseFixedType<JobMessage>());
                }));
    }
}