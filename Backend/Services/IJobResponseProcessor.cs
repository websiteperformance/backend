using Backend.Services.Inbound.JobResponse;

namespace Backend.Services;

public interface IJobResponseProcessor
{ 
    Task ProcessAsync(IAsyncEnumerable<JobMessageResult> jobMessages);
}