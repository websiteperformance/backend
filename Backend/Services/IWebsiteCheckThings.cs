using Backend.Endpoints.DoWebsiteCheck;
using Backend.Endpoints.ListWebsiteChecks;
using Backend.Objects;
using Backend.Services.Inbound.JobResponse;
using Backend.Services.Inbound.JobStorageResponse;
using Backend.Services.Outbound.JobRequest;

namespace Backend.Services;

public interface IWebsiteCheckThings
{
    Task<(int TotalPageCount, long TotalDocCount, IEnumerable<WebsiteCheckDocument> DataList)> GetWebsiteChecksFromUserIdAsync(ListWebsiteChecksDto dto);

    Task<JobMessageProducerResponse> InsertOneEntityAsync(WebsiteCheckDto request, Guid userId);

    Task<bool> UpdateJobResponseStateAsync(JobMessageResult jobMessageResult);

    Task<bool> UpdateJobStorageResponseStateAsync(PayloadResponseDto payloadResponseDto);

    Task<(bool Result, string Message)> DeleteOneEntityAsync(Guid siteId, Guid userId);

}