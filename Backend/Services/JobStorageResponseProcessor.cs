using Akka.Streams;
using Akka.Streams.Dsl;
using Akka.Streams.Implementation.Fusing;
using Backend.Services.Inbound.JobStorageResponse;

namespace Backend.Services;

public class JobStorageResponseProcessor(ILogger<JobStorageResponseProcessor> logger,
        IWebsiteCheckThings websiteCheckThings,
        IMaterializer materializer)
    : IJobStorageResponseProcessor
{
    public async Task ProcessAsync(IAsyncEnumerable<PayloadResponseDto> pAsyncEnumerable)
    {
        await Source.FromGraph(new AsyncEnumerable<PayloadResponseDto>(() => pAsyncEnumerable))
            .Where(d => d.ResponseId is not null)
            .SelectAsync(1, async entity =>
            {
                var result = await websiteCheckThings.UpdateJobStorageResponseStateAsync(entity);
                if (result)
                    logger.LogInformation("successfully store an id for storagetype {StorageType}",
                        entity.StorageType);
                return (entity.StorageType, result);
            })
            .Where(d => !d.result)
            .RunForeach(n => logger.LogWarning("cannot update {StorageType}", n.StorageType), materializer);
    }
}