using LanguageExt;

namespace Backend.Services;

public class ConfigurationHandler(IConfiguration configuration) : IConfigurationHandler
{
    public T GetValue<T>(string key)
    {
        return configuration.GetValue<T>(key) ??
               throw new ValueIsNullException($"value for key {key} cannot be found in configuration");
    }
}