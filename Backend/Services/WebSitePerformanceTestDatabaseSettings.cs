﻿namespace Backend.Services;

public class WebSitePerformanceTestDatabaseSettings
{
    public string? WebsitePerformanceTestDatabaseName { get; set; }
    
    public string? WebsitePerformanceTestConnectionString { get; set; }
    
    public string? WebsitePerformanceTestCollectionName { get; set; }
}