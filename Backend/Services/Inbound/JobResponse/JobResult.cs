using System.Text.Json.Serialization;

namespace Backend.Services.Inbound.JobResponse;

public class JobMessageResult
{
    [JsonInclude, JsonPropertyName("id")] public Guid Id;

    [JsonInclude, JsonPropertyName("cTime")]
    public DateTime CTime;

    [JsonInclude, JsonPropertyName("jobState")]
    public JobState? JobState;
}