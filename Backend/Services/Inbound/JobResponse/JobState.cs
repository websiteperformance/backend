using System.Text.Json.Serialization;

namespace Backend.Services.Inbound.JobResponse;

public record JobState
{
    [JsonInclude] public bool Screenshot;

    [JsonInclude] public bool Timings;

    [JsonInclude] public bool PerformanceEntries;

    [JsonInclude] public bool Tracings;

    [JsonInclude] public bool Networks;
}