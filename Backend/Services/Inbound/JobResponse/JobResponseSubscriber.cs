namespace Backend.Services.Inbound.JobResponse;

public class JobResponseSubscriber(ILogger<JobResponseSubscriber> logger, IJobResponseProcessor jobResponseProcessor)
{
    public async Task OnBatchReceivedAsync(IAsyncEnumerable<JobMessageResult> batch)
    {
        logger.LogInformation("receive a bunch of job responses, store the resuts");
        await jobResponseProcessor.ProcessAsync(batch);
    }
    
    
    
}