using System.Text.Json.Serialization;

namespace Backend.Services.Inbound.JobStorageResponse;

public class PayloadResponseDto
{
    [JsonInclude, JsonPropertyName("id")] public Guid Id;

    [JsonInclude, JsonPropertyName("responseId")]
    public Guid? ResponseId;

    [JsonInclude, JsonPropertyName("response_message")]
    public string? ResponseMessage;

    [JsonInclude, JsonPropertyName("storage_type")]
    public string? StorageType;
}