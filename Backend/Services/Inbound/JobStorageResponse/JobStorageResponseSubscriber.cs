namespace Backend.Services.Inbound.JobStorageResponse;

public class JobStorageResponseSubscriber(ILogger<JobStorageResponseSubscriber> logger,
    IJobStorageResponseProcessor jobStorageResponseProcessor)
{
    public async Task OnBatchReceivedAsync(IAsyncEnumerable<PayloadResponseDto> payload)
    {
        logger.LogInformation("receive a bunch of storage processed information");
        await jobStorageResponseProcessor.ProcessAsync(payload);
    }
    
}