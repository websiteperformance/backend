using Confluent.Kafka;
using Silverback.Messaging.Configuration;

namespace Backend.Services.Inbound.JobStorageResponse;

public class JobStorageResponseEndpointConfigurator(ILogger<JobStorageResponseEndpointConfigurator> logger,
        IConfigurationHandler configurationHandler)
    : IEndpointsConfigurator
{
    public void Configure(IEndpointsConfigurationBuilder builder)
    {
        builder
            .AddKafkaEndpoints(endpoints => endpoints
                .Configure(config =>
                {
                    config.BootstrapServers =
                        configurationHandler.GetValue<string>("KAFKA_BOOTSTRAP_SERVER");
                }).AddInbound(endpoint =>
                    endpoint
                        .ConsumeFrom(
                            configurationHandler.GetValue<string>("KAFKA_JOBSTORAGERESPONSE_TOPICNAME")).Configure(
                            config =>
                            {
                                config.GroupId =
                                    configurationHandler.GetValue<string>("KAFKA_JOBREQUESTS_GROUPID");
                                config.AutoOffsetReset = AutoOffsetReset.Latest;
                            })
                        .ValidateMessage(throwException: true)
                        .OnError(policy =>
                        {
                            logger.LogWarning("cannot process message");
                            policy.Skip();
                        })
                        .DeserializeJson(ser => ser.UseFixedType<PayloadResponseDto>())));
    }
}