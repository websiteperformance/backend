using Akka.Streams;
using Akka.Streams.Dsl;
using Akka.Streams.Implementation.Fusing;
using Backend.Services.Inbound.JobResponse;

namespace Backend.Services;

public class JobResponseProcessor(ILogger<JobResponseProcessor> logger, IWebsiteCheckThings websiteCheckThings,
        IMaterializer materializer)
    : IJobResponseProcessor
{
    public async Task ProcessAsync(IAsyncEnumerable<JobMessageResult> jobMessages)
    {
        await Source.FromGraph(new AsyncEnumerable<JobMessageResult>(() => jobMessages))
            .SelectAsync(1, async data =>
            {
                var result = await websiteCheckThings.UpdateJobResponseStateAsync(data);
                return (data.Id, result);
            })
            .Where(tuple => !tuple.result)
            .RunForeach(negative => logger.LogWarning("cannot update {Id}", negative.Id), materializer);
    }
}