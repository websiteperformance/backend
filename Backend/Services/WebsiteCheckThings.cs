using System.Text.Json;
using Backend.Endpoints.DoWebsiteCheck;
using Backend.Endpoints.ListWebsiteChecks;
using Backend.Objects;
using Backend.Services.Inbound.JobResponse;
using Backend.Services.Inbound.JobStorageResponse;
using Backend.Services.Outbound.JobRequest;
using Backend.Services.Outbound.RemoveFromStorage;
using LanguageExt;
using MongoDB.Driver;

namespace Backend.Services;

public class WebsiteCheckThings(
    ILogger<WebsiteCheckThings> logger,
    IDatabaseConnector<WebsiteCheckDocument> databaseConnector,
    IJobMessageProducer jobMessageProducer,
    IRemoveFromStorageProducer removeFromStorageProducer)
    : IWebsiteCheckThings
{
    public async Task<(int TotalPageCount, long TotalDocCount, IEnumerable<WebsiteCheckDocument> DataList)>
        GetWebsiteChecksFromUserIdAsync(ListWebsiteChecksDto dto)
    {
        logger.LogInformation("get all the checked websites from user Id");
        var filterDefinition = Builders<WebsiteCheckDocument>
            .Filter
            .Eq(d => d.UserId, dto.userId);

        if (dto.filterNetwork != null)
        {
            var networkFilter = Builders<WebsiteCheckDocument>.Filter.Eq(d => d.HasNetworks, dto.filterNetwork);
            filterDefinition &= networkFilter;
        }

        if (dto.filterScreenshot != null)
        {
            var screenshotFilter = Builders<WebsiteCheckDocument>.Filter.Eq(d => d.HasScreenShot, dto.filterScreenshot);
            filterDefinition &= screenshotFilter;
        }

        if (dto.filterTiming != null)
        {
            var timingFilter = Builders<WebsiteCheckDocument>.Filter.Eq(d => d.HasTimings, dto.filterTiming);
            filterDefinition &= timingFilter;
        }

        if (dto.filterTracing != null)
        {
            var tracingFilter = Builders<WebsiteCheckDocument>.Filter.Eq(d => d.HasTracing, dto.filterTracing);
            filterDefinition &= tracingFilter;
        }

        if (dto.filterPerformanceEntries != null)
        {
            var performanceEntriesFilter =
                Builders<WebsiteCheckDocument>.Filter.Eq(d => d.HasPerformanceEntries, dto.filterPerformanceEntries);
            filterDefinition &= performanceEntriesFilter;
        }

        var sort = Builders<WebsiteCheckDocument>.Sort.Descending(x => x.CreateDate);

        return await AggregateResult(logger, databaseConnector.MongoCollection, filterDefinition, sort, dto.page,
            dto.pageSize);
    }

    private static readonly Func<ILogger, IMongoCollection<WebsiteCheckDocument>, FilterDefinition<WebsiteCheckDocument>
        , SortDefinition<WebsiteCheckDocument>, int, int,
        Task<(int TotalPageCount, long TotalDocCount, IEnumerable<WebsiteCheckDocument> Data)>> AggregateResult =
        async (logger, mongoCollection, filter, sort, page, pageSize) =>
        {
            var countFacet = AggregateFacet.Create("count",
                PipelineDefinition<WebsiteCheckDocument, AggregateCountResult>.Create(new[]
                {
                    PipelineStageDefinitionBuilder.Count<WebsiteCheckDocument>()
                }));

            var dataFacet = AggregateFacet.Create("data",
                PipelineDefinition<WebsiteCheckDocument, WebsiteCheckDocument>.Create(new[]
                {
                    PipelineStageDefinitionBuilder.Sort(sort),
                    PipelineStageDefinitionBuilder.Skip<WebsiteCheckDocument>((page - 1) * pageSize),
                    PipelineStageDefinitionBuilder.Limit<WebsiteCheckDocument>(pageSize),
                }));

            var aggregation = await mongoCollection
                .Aggregate()
                .Match(filter)
                .Facet(countFacet, dataFacet)
                .ToListAsync();

            var count = aggregation.Count > 0 && aggregation[0]
                .Facets
                .First(x => x.Name == "count")?
                .Output<AggregateCountResult>().Count > 0
                ? aggregation[0]
                    .Facets
                    .First(x => x.Name == "count")?
                    .Output<AggregateCountResult>()[0]
                    .Count ?? 0
                : 0;

            var totalPages = (count > 0) ? (int) Math.Ceiling((double) count / pageSize) : 0;

            logger.LogInformation("receive a message count {MessageCount}", count);
            logger.LogInformation("get with page {Page}", page);
            logger.LogInformation("get with pageSize {PageSize}", pageSize);
            logger.LogInformation("calculated totalPages {TotalPages}", totalPages);

            var data = aggregation
                .First()
                .Facets
                .First(x => x.Name == "data")
                .Output<WebsiteCheckDocument>();

            return (totalPages, count, data);
        };


    public async Task<JobMessageProducerResponse> InsertOneEntityAsync(WebsiteCheckDto request, Guid userId)
    {
        logger.LogInformation("receive a request for storing a website check in database");
        WebsiteCheckDocument entity = request;
        entity.UserId = userId;
        entity.SiteCheckId = Guid.NewGuid();
        entity.CreateDate = DateTime.Now;
        await databaseConnector.InsertOneAsync(entity);

        JobMessage jobMessage = entity;
        var result = await jobMessageProducer.ProduceJobAsync(jobMessage);
        return result;
    }

    public async Task<bool> UpdateJobResponseStateAsync(JobMessageResult jobMessageResult)
    {
        logger.LogInformation("update an entity when the runner send the job result");
        var filterDefinition = Builders<WebsiteCheckDocument>.Filter.Eq(d => d.SiteCheckId, jobMessageResult.Id);

        var updateDefinition = Builders<WebsiteCheckDocument>
            .Update
            .Set(d => d.HasTimings, jobMessageResult.JobState?.Timings ?? false)
            .Set(d => d.HasScreenShot, jobMessageResult.JobState?.Screenshot ?? false)
            .Set(d => d.HasPerformanceEntries, jobMessageResult.JobState?.PerformanceEntries ?? false)
            .Set(d => d.HasTracing, jobMessageResult.JobState?.Tracings ?? false)
            .Set(d => d.HasNetworks, jobMessageResult.JobState?.Networks ?? false);
        var updateResult = await databaseConnector.UpdateOneAsync(filterDefinition, updateDefinition);
        logger.LogInformation("mongodb update is {UpdateResult}", updateResult.IsAcknowledged);
        return updateResult.IsAcknowledged;
    }

    public async Task<bool> UpdateJobStorageResponseStateAsync(PayloadResponseDto payloadResponseDto)
    {
        logger.LogInformation("update an entity when the objectstorage send the storage result");
        var filterDefinition = Builders<WebsiteCheckDocument>.Filter.Eq(d => d.SiteCheckId, payloadResponseDto.Id);
        var updateDefinition = payloadResponseDto.StorageType switch
        {
            "timings" => Builders<WebsiteCheckDocument>.Update.Set(d => d.TimingsId, payloadResponseDto.ResponseId),
            "screenshots" => Builders<WebsiteCheckDocument>.Update.Set(d => d.ScreenshotId,
                payloadResponseDto.ResponseId),
            "performanceentries" => Builders<WebsiteCheckDocument>.Update.Set(d => d.PerformanceEntriesId,
                payloadResponseDto.ResponseId),
            "tracings" => Builders<WebsiteCheckDocument>.Update.Set(d => d.TracingId, payloadResponseDto.ResponseId),
            "networks" => Builders<WebsiteCheckDocument>.Update.Set(d => d.NetworkId, payloadResponseDto.ResponseId),
            _ => throw new InvalidOperationException(
                $"not possible to update appropriate storage type {payloadResponseDto.StorageType}")
        };

        var updateResult = await databaseConnector.UpdateOneAsync(filterDefinition, updateDefinition);
        logger.LogInformation("mongodb update is {UpdateResult}", updateResult.IsAcknowledged);
        return updateResult.IsAcknowledged;
    }


    public async Task<(bool Result, string Message)> DeleteOneEntityAsync(Guid siteId, Guid userId)
    {
        logger.LogInformation("try to remove all data for web check {Id} for userId {UserId}", siteId, userId);
        var filterDefinition =
            Builders<WebsiteCheckDocument>.Filter.Eq(d => d.SiteCheckId, siteId) &
            Builders<WebsiteCheckDocument>.Filter.Eq(d => d.UserId, userId);

        var resultOpt = await databaseConnector.FindOneAsync(filterDefinition);

        return await resultOpt.MatchAsync(
            Some: async result =>
            {
                logger.LogInformation("found a document to remove, document data is:");
                var json = JsonSerializer.Serialize(result,
                    new JsonSerializerOptions {WriteIndented = true, IncludeFields = true});

                logger.LogInformation("data is {Data}", json);

                (IRemoveFromStorageProducer producer, ILogger logger, bool hasValue, Guid storageId,
                    PayloadTypeEnum type, Func<IRemoveFromStorageProducer, ILogger, bool, Guid, PayloadTypeEnum,
                        Task<Option<RemoveFromStorageProducerResponse>>> fnc)[] tr =
                    [
                        (removeFromStorageProducer, logger, result.HasTimings, result.TimingsId, PayloadTypeEnum.Timing,
                            PutRemoveMessage),
                        (removeFromStorageProducer, logger, result.HasPerformanceEntries, result.PerformanceEntriesId,
                            PayloadTypeEnum.PerformanceEntry, PutRemoveMessage),
                        (removeFromStorageProducer, logger, result.HasScreenShot, result.ScreenshotId,
                            PayloadTypeEnum.Screenshot, PutRemoveMessage),
                        (removeFromStorageProducer, logger, result.HasTracing, result.TracingId,
                            PayloadTypeEnum.Tracing, PutRemoveMessage),
                        (removeFromStorageProducer, logger, result.HasNetworks, result.NetworkId,
                            PayloadTypeEnum.Networks, PutRemoveMessage)
                    ];

                var seqTasks =
                    await tr.WhenAllFuncAsync(
                        async valueTuple => await valueTuple.fnc(removeFromStorageProducer, logger, valueTuple.hasValue,
                            valueTuple.storageId, valueTuple.type), 4);

                var returnString = seqTasks
                    .Somes()
                    .Filter(d => d.IsError)
                    .Map(e => $"{e.ErrorMessage} :: {e.Id}")
                    .Join(", ");

                if (returnString.IsNullOrEmpty2())
                {
                    var fd = Builders<WebsiteCheckDocument>.Filter.Eq(d => d.SiteCheckId, result.SiteCheckId);
                    var deleteResult = await databaseConnector.DeleteManyAsync(fd);
                    if (deleteResult.IsAcknowledged)
                    {
                        logger.LogInformation("successfully deleted a check with id {SiteCheckId}",
                            result.SiteCheckId);
                    }
                    else
                    {
                        logger.LogWarning("something went wrong while deleting a site check with id {SiteCheckId}",
                            result.SiteCheckId);
                    }
                }
                else
                {
                    logger.LogWarning("something went wrong while putting delete messages to topic");
                    logger.LogWarning("{ReturnString}", returnString);
                }

                return await Task.FromResult((returnString.Length == 0, str: returnString));

                static async Task<Option<RemoveFromStorageProducerResponse>> PutRemoveMessage(
                    IRemoveFromStorageProducer removeFromStorageProducer, ILogger logger, bool hasValue, Guid id,
                    PayloadTypeEnum type)
                {
                    logger.LogInformation("call into PutRemoveMessage");

                    if (!hasValue)
                    {
                        logger.LogWarning("nothing to remove, hasValue is false");
                        return Option<RemoveFromStorageProducerResponse>.None;
                    }

                    var dto = new PayloadDto {Id = id, PayloadTypeEnum = type};
                    var resultResponse = await removeFromStorageProducer.ProduceJobAsync(dto);
                    if (!resultResponse.IsError)
                    {
                        return resultResponse;
                    }

                    logger.LogWarning("cannot put id {Id} in topic for deletion a {Type}", resultResponse.Id, type);
                    logger.LogWarning("{Message}", resultResponse.ErrorMessage);

                    return resultResponse;
                }
            },
            None: async () =>
            {
                logger.LogWarning("cannot find any website check for given id {Id}", siteId);
                return await Task.FromResult((false,
                    $"cannot find any website check for given id {siteId.ToString()}"));
            });
    }
}