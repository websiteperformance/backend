using System.Diagnostics.Contracts;

namespace Backend.Services;

public static class Utils
{
    [Pure]
    public static bool IsNullOrEmpty2(this string? value) => string.IsNullOrEmpty(value);

    [Pure]
    public static string Join(this IEnumerable<string> value, string separator) => string.Join(separator, value);

    public static async Task ProcessWhenReady<T>(this List<Task<T>> source)
    {
        while (source.Any())
        {
            var fT = await Task.WhenAny(source);
            source.Remove(fT);
            await fT;
        }
    } 
    
    
    [Pure]
    public static async Task<IEnumerable<T>> WhenAllBackPressure<T>(this IEnumerable<Task<T>> source, int parallelism)
    {
        var semaphore = new SemaphoreSlim(parallelism);

        var tasks = source.Select(async item =>
        {
            await semaphore.WaitAsync();
            return await item.ContinueWith(t =>
            {
                semaphore.Release();
                if (t.IsFaulted) throw t.Exception;
                return t.Result;
            });
        });
        return await Task.WhenAll(tasks);
    }

    [Pure]
    public static async Task<IEnumerable<TResult>> WhenAllFuncAsync<T, TResult>(this IEnumerable<T> source,
        Func<T, Task<TResult>> func,
        int parallelism)
    {
        var semaphore = new SemaphoreSlim(parallelism);
        var tasks = source.Select(async item =>
        {
            await semaphore.WaitAsync();
            try
            {
                return await func.Invoke(item);
            }
            finally
            {
                semaphore.Release();
            }
        });

        return await Task.WhenAll(tasks);
    }
}