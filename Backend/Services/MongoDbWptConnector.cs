using LanguageExt;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Backend.Services;

public class MongoDbWptConnector<T> : IDatabaseConnector<T>
{
    private readonly ILogger<MongoDbWptConnector<T>> _logger;

    public IMongoCollection<T> MongoCollection { get; }

    public MongoDbWptConnector(ILogger<MongoDbWptConnector<T>> logger, IOptions<WebSitePerformanceTestDatabaseSettings> databaseSettings)
    {
        _logger = logger;
        var mongoClient = new MongoClient(databaseSettings.Value.WebsitePerformanceTestConnectionString);
        var mongoDatabase = mongoClient.GetDatabase(databaseSettings.Value.WebsitePerformanceTestDatabaseName);
        MongoCollection = mongoDatabase.GetCollection<T>(databaseSettings.Value.WebsitePerformanceTestCollectionName);
    }

    public Task<IEnumerable<T>> FindManyAsync(FilterDefinition<T> filterDefinition)
    {
        _logger.LogInformation("find many entities");
        return Task.FromResult(MongoCollection.Find(filterDefinition).ToEnumerable());
    }

    public async Task<Option<T>> FindOneAsync(FilterDefinition<T> filterDefinition)
    {
        _logger.LogInformation("find one entity");
        return await MongoCollection.Find(filterDefinition).FirstAsync();
    }

    public Task InsertOneAsync(T value)
    {
        _logger.LogInformation("insert one entity");
        return MongoCollection.InsertOneAsync(value);
    }

    public Task<UpdateResult> UpdateOneAsync(FilterDefinition<T> filterDefinition, UpdateDefinition<T> newValue)
    {
        _logger.LogInformation("update one entity");
        return MongoCollection.UpdateOneAsync(filterDefinition, newValue);
    }

    public Task<DeleteResult> DeleteOneAsync(FilterDefinition<T> filterDefinition)
    {
        _logger.LogInformation("delete one entity");
        return MongoCollection.DeleteOneAsync(filterDefinition);
    }

    public Task<DeleteResult> DeleteManyAsync(FilterDefinition<T> filterDefinition)
    {
        _logger.LogInformation("delete many entities");
        return MongoCollection.DeleteManyAsync(filterDefinition);
    }
}