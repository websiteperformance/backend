using Ardalis.ApiEndpoints;
using Backend.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Backend.Endpoints.DeleteWebsiteCheck;

public class DeleteWebsiteCheckEndpoint(ILogger<DeleteWebsiteCheckEndpoint> logger,
        IWebsiteCheckThings websiteCheckThings)
    : EndpointBaseAsync.WithRequest<DeleteWebsiteCheckRequest>.WithActionResult
{
    [HttpDelete("/api/checks/delete/{userId}/{checkId}")]
    [SwaggerOperation(
        Summary = "delete a website check",
        Description = "delete a website check and appropriate results for a given id",
        OperationId = "cc1071eb-6b79-4f40-8dbe-7bf25307ed8a",
        Tags = new[] {"api"}
    )]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public override async Task<ActionResult> HandleAsync([FromRoute] DeleteWebsiteCheckRequest request,
        CancellationToken cancellationToken = new CancellationToken())
    {
        logger.LogInformation("try to delete a stored website check and the appropriated data in storage");
        var result = await websiteCheckThings.DeleteOneEntityAsync(request.CheckId, request.UserId);
        return result.Result ? Ok($"successfully removed test with id {request.CheckId}") : Ok(result.Message);

    }
}