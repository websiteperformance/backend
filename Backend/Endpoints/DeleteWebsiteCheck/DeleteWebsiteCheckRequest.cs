using Microsoft.AspNetCore.Mvc;

namespace Backend.Endpoints.DeleteWebsiteCheck;

public class DeleteWebsiteCheckRequest
{
    [FromRoute(Name = "checkId")] public Guid CheckId { get; set; }

    [FromRoute(Name = "userId")] public Guid UserId { get; set; }
}