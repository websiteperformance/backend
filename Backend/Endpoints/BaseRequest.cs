using System.Text.Json.Serialization;

namespace Backend.Endpoints;

public class BaseRequest
{
    [JsonInclude, JsonPropertyName("userId"), JsonRequired]
    public Guid UserId;
}