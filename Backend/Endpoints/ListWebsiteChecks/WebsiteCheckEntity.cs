using System.Text.Json.Serialization;
using Backend.Endpoints.DoWebsiteCheck;
using Backend.Objects;
using Newtonsoft.Json;

namespace Backend.Endpoints.ListWebsiteChecks;

public class WebsiteCheckEntity
{
    [JsonInclude, JsonPropertyName("id")] public Guid Id;

    [JsonInclude, JsonPropertyName("userId")]
    public Guid UserId;

    [JsonInclude, JsonPropertyName("url")] public string? Url;

    [JsonInclude, JsonPropertyName("location")]
    public Location? Location;

    [JsonInclude, JsonPropertyName("ctime")]
    public DateTime CreateDate;

    [JsonInclude, JsonPropertyName("width")]
    public int Width;

    [JsonInclude, JsonPropertyName("height")]
    public int Height;

    [JsonInclude, JsonPropertyName("userAgent")]
    public string? UserAgent;

    [JsonInclude, JsonPropertyName("hasScreenshot")]
    public bool HasScreenshot;

    [JsonInclude, JsonPropertyName("hasTimings")]
    public bool HasTimings;

    [JsonInclude, JsonPropertyName("hasPerformanceEntries")]
    public bool HasPerformanceEntries;

    [JsonInclude, JsonPropertyName("hasTracing")]
    public bool HasTracing;

    [JsonInclude, JsonPropertyName("hasNetwork")]
    public bool HasNetwork;

    [JsonInclude, JsonPropertyName("screenshotId")]
    public Guid ScreenshotId;

    [JsonInclude, JsonPropertyName("timingsId")]
    public Guid TimingsId;

    [JsonInclude, JsonPropertyName("performanceEntriesId")]
    public Guid PerformanceEntriesId;

    [JsonInclude, JsonPropertyName("tracingId")]
    public Guid TracingId;

    [JsonInclude, JsonPropertyName("networkId")]
    public Guid NetworkId;

    [JsonInclude, JsonPropertyName("withTimings")]
    public bool WithTimings;

    [JsonInclude, JsonPropertyName("withPerformanceEntries")]
    public bool WithPerformanceEntries;

    [JsonInclude, JsonPropertyName("withScreenshot")]
    public bool WithScreenshot;

    [JsonInclude, JsonPropertyName("withTracing")]
    public bool WithTracing;

    [JsonInclude, JsonPropertyName("withNetworks")]
    public bool WithNetworks;

    public static implicit operator WebsiteCheckEntity(WebsiteCheckDocument doc) =>
        new()
        {
            Id = doc.SiteCheckId, Url = doc.Url, CreateDate = doc.CreateDate, HasScreenshot = doc.HasScreenShot,
            HasTimings = doc.HasTimings, HasPerformanceEntries = doc.HasPerformanceEntries,
            ScreenshotId = doc.ScreenshotId, TimingsId = doc.TimingsId, PerformanceEntriesId = doc.PerformanceEntriesId,
            Height = doc.Height, Width = doc.Width, UserAgent = doc.UserAgent, Location = doc.Location,
            UserId = doc.UserId, TracingId = doc.TracingId, HasTracing = doc.HasTracing, WithTracing = doc.WithTracing,
            WithTimings = doc.WithTimings, WithScreenshot = doc.WithScreenshot,
            WithPerformanceEntries = doc.HasPerformanceEntries, WithNetworks = doc.WithNetworks,
            HasNetwork = doc.HasNetworks, NetworkId = doc.NetworkId
        };
}