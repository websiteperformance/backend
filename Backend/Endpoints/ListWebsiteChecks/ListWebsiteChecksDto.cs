namespace Backend.Endpoints.ListWebsiteChecks;

public record ListWebsiteChecksDto(
    Guid userId,
    int page,
    int pageSize,
    bool? filterScreenshot,
    bool? filterTiming,
    bool? filterPerformanceEntries,
    bool? filterTracing,
    bool? filterNetwork)
{
    public static implicit operator ListWebsiteChecksDto(ListWebsiteCheckRequest from) => new(from.UserId, from.Page,
        from.PageSize, from.FilterScreenshot,
        from.FilterTiming, from.FilterPerformanceEntries, from.FilterTracing, from.FilterNetwork);
}