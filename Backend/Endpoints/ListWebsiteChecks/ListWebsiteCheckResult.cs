using System.Text.Json.Serialization;

namespace Backend.Endpoints.ListWebsiteChecks;

public class ListWebsiteCheckResult
{
    [JsonPropertyName("pageCount"), JsonInclude]
    public int PageCount;

    [JsonPropertyName("docCount"), JsonInclude]
    public long DocCount;
    
    [JsonInclude, JsonPropertyName("websiteList")]
    public IEnumerable<WebsiteCheckEntity>? WebsiteList;

    [JsonInclude, JsonPropertyName("isError")]
    public bool IsError;

    [JsonInclude, JsonPropertyName("errorMessage")]
    public string? ErrorMessage;
    
}