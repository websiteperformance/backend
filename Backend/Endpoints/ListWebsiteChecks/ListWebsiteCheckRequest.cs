using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Endpoints.ListWebsiteChecks;

public class ListWebsiteCheckRequest
{
    [FromRoute(Name = "userId"), Required] public Guid UserId { get; set; }

    [FromRoute(Name = "page"), Required, Range(1, 100)]
    public int Page { get; set; }

    [FromRoute(Name = "pageSize"), Required, Range(1, double.MaxValue)]
    public int PageSize { get; set; }

    [FromQuery(Name = "filterScreenshot")] public bool? FilterScreenshot { get; set; }

    [FromQuery(Name = "filterTiming")] public bool? FilterTiming { get; set; }

    [FromQuery(Name = "filterPerformanceEntries")]
    public bool? FilterPerformanceEntries { get; set; }

    [FromQuery(Name = "filterTracing")] public bool? FilterTracing { get; set; }

    [FromQuery(Name = "filterNetwork")] public bool? FilterNetwork { get; set; }
}