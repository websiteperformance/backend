using Ardalis.ApiEndpoints;
using Backend.Services;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Backend.Endpoints.ListWebsiteChecks;

public class
    ListWebsiteChecksEndpoint(
        ILogger<ListWebsiteChecksEndpoint> logger,
        IWebsiteCheckThings websiteCheckThings)
    : EndpointBaseAsync.WithRequest<ListWebsiteCheckRequest>.WithActionResult<
        ListWebsiteCheckResult>
{
    [HttpGet("/api/checks/list/{userId}/{page}/{pageSize}")]
    [SwaggerOperation(
        Summary = "website checks per user",
        Description = "get list of all website checks of the current user",
        OperationId = "ec0d62b4-ef18-4e5c-9885-90881b4891b",
        Tags = new[] {"api"}
    )]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public override async Task<ActionResult<ListWebsiteCheckResult>> HandleAsync(
        [FromRoute] ListWebsiteCheckRequest requestObject,
        CancellationToken cancellationToken = new())
    {
        logger.LogInformation("try to load all checks for a user");
        var url = Request.GetEncodedUrl();
        logger.LogInformation("receive full url {Url}", url);
            
        var entities =
            await websiteCheckThings.GetWebsiteChecksFromUserIdAsync(requestObject);
        var returnResult = new ListWebsiteCheckResult
        {
            WebsiteList = entities.DataList.Select(d => (WebsiteCheckEntity) d), DocCount = entities.TotalDocCount,
            PageCount = entities.TotalPageCount
        };
        return Ok(returnResult);
    }
}