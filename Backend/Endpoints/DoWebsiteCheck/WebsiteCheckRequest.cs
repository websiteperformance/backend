using System.ComponentModel;
using System.Text.Json.Serialization;

namespace Backend.Endpoints.DoWebsiteCheck;

public class WebsiteCheckRequest : BaseRequest
{
    [JsonInclude, JsonPropertyName("url")] public string? Url;

    [JsonInclude, JsonPropertyName("location")]
    public int Location;

    [JsonInclude, JsonPropertyName("width"), DefaultValue(1920)]
    public int Width;

    [JsonInclude, JsonPropertyName("height"), DefaultValue(1080)]
    public int Height;

    [JsonInclude, JsonPropertyName("userAgent"),
     DefaultValue(
         "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36")]
    public string? UserAgent;

    [JsonInclude, JsonPropertyName("withTimings"), DefaultValue(true)]
    public bool WithTimings;

    [JsonInclude, JsonPropertyName("withPerformanceEntries"), DefaultValue(true)]
    public bool WithPerformanceEntries;

    [JsonInclude, JsonPropertyName("withScreenshot"), DefaultValue(true)]
    public bool WithScreenshot;

    [JsonInclude, JsonPropertyName("withTracing"), DefaultValue(true)]
    public bool WithTracing;

    [JsonInclude, JsonPropertyName("withNetworks"), DefaultValue(true)]
    public bool WithNetworks;

}