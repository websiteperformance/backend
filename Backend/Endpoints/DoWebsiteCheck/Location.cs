namespace Backend.Endpoints.DoWebsiteCheck;

public enum Location
{
    Home = 1,
    Frankfurt = 2
}
