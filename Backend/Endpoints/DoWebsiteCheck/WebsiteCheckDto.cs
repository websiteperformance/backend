using System.ComponentModel;
using System.Text.Json.Serialization;

namespace Backend.Endpoints.DoWebsiteCheck;

public class WebsiteCheckDto
{
    public string? Url;
    public int Location;
    public int Width;
    public int Height;
    public string? UserAgent;
    public bool WithTimings;
    public bool WithScreenshot;
    public bool WithPerformanceEntries;
    public bool WithTracing;
    public bool WithNetworks;

    public static implicit operator WebsiteCheckDto(WebsiteCheckRequest websiteCheckRequest) => new()
    {
        Url = websiteCheckRequest.Url,
        Height = websiteCheckRequest.Height,
        Location = websiteCheckRequest.Location,
        UserAgent = websiteCheckRequest.UserAgent,
        Width = websiteCheckRequest.Width,
        WithTimings = websiteCheckRequest.WithTimings,
        WithTracing = websiteCheckRequest.WithTracing,
        WithScreenshot = websiteCheckRequest.WithScreenshot,
        WithPerformanceEntries = websiteCheckRequest.WithPerformanceEntries,
        WithNetworks = websiteCheckRequest.WithNetworks
    };
}