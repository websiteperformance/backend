using System.Text.Json.Serialization;
using Backend.Services.Outbound.JobRequest;

namespace Backend.Endpoints.DoWebsiteCheck;

public class WebsiteCheckResponse
{
    [JsonInclude, JsonPropertyName("id")] public Guid Id;

    [JsonInclude, JsonPropertyName("active")]
    public bool Active;

    [JsonInclude, JsonPropertyName("errorMessage")]
    public string? ErrorMessage;


    public static implicit operator WebsiteCheckResponse(JobMessageProducerResponse response) => new()
    {
        Id = response.Id,
        ErrorMessage = response.ErrorMessage, Active = response.IsError
    };

}