using Ardalis.ApiEndpoints;
using Backend.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Backend.Endpoints.DoWebsiteCheck;

public class
    DoWebsiteCheckEndpoint(ILogger<DoWebsiteCheckEndpoint> logger,
        IWebsiteCheckThings websiteCheckThings)
    : EndpointBaseAsync.WithRequest<WebsiteCheckRequest>.WithActionResult<WebsiteCheckResponse>
{
    [HttpPut("/api/checks/test")]
    [SwaggerOperation(
        Summary = "receive website check",
        Description = "receive appropriate information to process a website check",
        OperationId = "06e32b42-2644-464c-a0a3-cf0bc6d073a1",
        Tags = new[] {"api"}
    )]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public override async Task<ActionResult<WebsiteCheckResponse>> HandleAsync(WebsiteCheckRequest request,
        CancellationToken cancellationToken = new())
    {
        logger.LogInformation("create a webcheck for an url");

        var response = await websiteCheckThings.InsertOneEntityAsync(request, request.UserId);
        return Ok(response);
    }
}