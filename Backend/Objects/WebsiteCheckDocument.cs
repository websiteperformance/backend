using Backend.Endpoints.DoWebsiteCheck;
using MongoDB.Bson;

namespace Backend.Objects;

public class WebsiteCheckDocument
{
    public ObjectId Id { get; set; }

    public string? Url { get; set; }

    public DateTime CreateDate { get; set; }

    public Guid UserId { get; set; }

    public int Width { get; set; }

    public int Height { get; set; }

    public string? UserAgent { get; set; }

    public Location Location { get; set; }

    public Guid SiteCheckId { get; set; }

    public bool HasScreenShot { get; set; }

    public bool HasTimings { get; set; }

    public bool HasPerformanceEntries { get; set; }

    public bool HasTracing { get; set; }
    
    public bool HasNetworks { get; set; }

    public Guid ScreenshotId { get; set; }

    public Guid TimingsId { get; set; }

    public Guid TracingId { get; set; }
    
    public Guid NetworkId { get; set; }

    public Guid PerformanceEntriesId { get; set; }

    public bool WithTimings { get; set; }

    public bool WithPerformanceEntries { get; set; }

    public bool WithScreenshot { get; set; }

    public bool WithTracing { get; set; }
    
    public bool WithNetworks { get; set; }

    public static implicit operator WebsiteCheckDocument(WebsiteCheckDto request) => new()
    {
        Url = request.Url,
        Width = request.Width,
        Height = request.Height,
        UserAgent = request.UserAgent,
        Location = (Location) request.Location,
        WithScreenshot = request.WithScreenshot,
        WithTracing = request.WithTracing,
        WithPerformanceEntries = request.WithPerformanceEntries,
        WithTimings = request.WithTimings,
        WithNetworks = request.WithNetworks
    };
}