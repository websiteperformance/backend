using System.Text.Json.Serialization;

namespace Backend.Health;

public class HealthEndpointResult
{
    [JsonInclude, JsonPropertyName("status")]
    public string? Status;
}