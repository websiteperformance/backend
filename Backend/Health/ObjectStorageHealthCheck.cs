using System.Net;
using System.Text.Json;
using Backend.Services;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Backend.Health;

public class ObjectStorageHealthCheck(IConfigurationHandler configurationHandler,
        ILogger<ObjectStorageHealthCheck> logger, IHttpClientFactory httpClientFactory)
    : IHealthCheck
{
    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
        CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var checkUrlAddress = configurationHandler.GetValue<string>("OBJECTSTORAGE_REQUEST_URI");
            var checkUrl = $"{checkUrlAddress}/healthz";
            var client = httpClientFactory.CreateClient();
            var result = await client.GetAsync(checkUrl, cancellationToken);

            if (result.StatusCode != HttpStatusCode.OK)
            {
                logger.LogWarning("unwanted status code of healthcheck {StatusCode}", result.StatusCode);
                return new HealthCheckResult(context.Registration.FailureStatus = HealthStatus.Degraded,
                    "unwanted status code");
            }

            var resultStream = await result.Content.ReadAsStreamAsync(cancellationToken);
            var resultObject = JsonSerializer.Deserialize<HealthEndpointResult>(resultStream);

            if (resultObject?.Status is null)
            {
                logger.LogWarning("content or property status is null");
                return new HealthCheckResult(context.Registration.FailureStatus = HealthStatus.Degraded,
                    "content or property is null");
            }

            if (resultObject.Status != "Healthy")
            {
                logger.LogWarning("status from remote healthcheck is not healthy, result is {ResultCode}",
                    resultObject.Status);
                return new HealthCheckResult(context.Registration.FailureStatus = HealthStatus.Degraded,
                    "unwanted status");
            }

            logger.LogInformation("everything is fine");
            return new HealthCheckResult(context.Registration.FailureStatus = HealthStatus.Healthy);
        }
        catch (Exception exception)
        {
            logger.LogError(exception, "an error occured");
            return await Task.FromResult(new HealthCheckResult(context.Registration.FailureStatus,
                "failure while reaching objectstorage endpoint", exception));
        }
    }
}