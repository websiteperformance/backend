using Akka.Actor;
using Akka.Streams;
using Backend.Health;
using Backend.Objects;
using Backend.Services;
using Backend.Services.Inbound.JobResponse;
using Backend.Services.Inbound.JobStorageResponse;
using Backend.Services.Outbound.JobRequest;
using Backend.Services.Outbound.RemoveFromStorage;
using Backend.Telemetry;
using HealthChecks.UI.Client;
using LanguageExt;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging.Console;
using Microsoft.OpenApi.Models;
const string myAllowSpecificOrigins = "_myAllowSpecificOrigins";
var builder = WebApplication.CreateSlimBuilder(args);
builder.Logging.AddSimpleConsole(options =>
{
    options.IncludeScopes = false;
    options.SingleLine = true;
    options.TimestampFormat = "[yyy-MM-dd HH:mm:ss.ffff]";
    options.ColorBehavior = LoggerColorBehavior.Enabled;
});
builder.Logging.AddFilter("*", LogLevel.Information);

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: myAllowSpecificOrigins,
        policy =>
        {
            policy
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
        });
});

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSingleton<IConfigurationHandler, ConfigurationHandler>();

builder.Services.Configure<WebSitePerformanceTestDatabaseSettings>(d =>
{
    d.WebsitePerformanceTestDatabaseName = builder.Configuration.GetValue<string>("MONGODB_WPT_DATABASENAME");
    d.WebsitePerformanceTestCollectionName = builder.Configuration.GetValue<string>("MONGODB_WPT_COLLECTIONNAME");
    d.WebsitePerformanceTestConnectionString = builder.Configuration.GetValue<string>("MONGODB_CONNECTIONSTRING");
});
builder.Services.AddHttpClient();
builder.Services.AddSingleton<IDatabaseConnector<WebsiteCheckDocument>, MongoDbWptConnector<WebsiteCheckDocument>>();
builder.Services.AddScoped<IWebsiteCheckThings, WebsiteCheckThings>();
builder.Services.AddScoped<IJobMessageProducer, JobMessageProducer>();
builder.Services.AddScoped<IJobResponseProcessor, JobResponseProcessor>();
builder.Services.AddScoped<IJobStorageResponseProcessor, JobStorageResponseProcessor>();
builder.Services.AddScoped<IRemoveFromStorageProducer, RemoveFromStorageProducer>();

builder.Services.AddSwaggerGenNewtonsoftSupport();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v2", new OpenApiInfo {Title = "Backend", Version = "v2"});
    c.EnableAnnotations();
});

var actorSystem = ActorSystem.Create("Backend");
builder.Services.AddSingleton<IMaterializer>(actorSystem.Materializer());

builder.Services.AddSilverback()
    .UseModel()
    .WithConnectionToMessageBroker(o => o.AddKafka())
    .AddEndpointsConfigurator<JobRequestsEndpointConfigurator>()
    .AddEndpointsConfigurator<JobResponseEndpointConfigurator>()
    .AddEndpointsConfigurator<JobStorageResponseEndpointConfigurator>()
    .AddEndpointsConfigurator<RemoveFromStorageEndpointConfigurator>()
    .AddScopedSubscriber<JobResponseSubscriber>()
    .AddScopedSubscriber<JobStorageResponseSubscriber>();

builder.Services.AddHealthChecks()
    .AddKafka(config =>
    {
        config.BootstrapServers = builder.Configuration.GetValue<string>("KAFKA_BOOTSTRAP_SERVER") ?? throw new ValueIsNullException("KAFKA_BOOTSTRAP_SERVER should not be null");
    }, failureStatus: HealthStatus.Unhealthy, timeout: TimeSpan.FromSeconds(2), name: "KafkaHealthCheck");

var app = builder.Build();
MethodTimeLogger.Logger = app.Logger;
app.Lifetime.ApplicationStarted.Register(() => { app.Services.GetService<ActorSystem>(); });
app.Lifetime.ApplicationStopping.Register(() => { app.Services.GetService<ActorSystem>()?.Terminate().Wait(); });
app.UseStaticFiles();
app.UseHttpsRedirection();

app.MapHealthChecks("/healthz", new HealthCheckOptions
{
    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
});

if (app.Environment.IsDevelopment())
{
    app.UseSwagger(c => { c.SerializeAsV2 = true; });
    app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v2/swagger.json", "Backend"); });
}

app.UseAuthorization();
app.MapControllers();

app.UseCors(x =>
    x
        .AllowAnyMethod()
        .AllowAnyHeader()
        .SetIsOriginAllowed(_ => true)
        .AllowAnyOrigin()
);

await app.RunAsync();